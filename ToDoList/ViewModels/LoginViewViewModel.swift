//
//  LoginViewViewModel.swift
//  ToDoList
//
//  Created by Ramya H G on 07/03/24.
//

import FirebaseAuth
import Foundation

class LoginViewViewModel: ObservableObject {
  @Published var email = ""
  @Published var password = ""
  @Published var errormessage = ""
  init() {
  }

  func login() {
    guard validate() else {
      return
    }

    // Try log in

    Auth.auth().signIn(withEmail: email, password: password)
  }

  func validate() -> Bool {
    errormessage = ""
    guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
      !password.trimmingCharacters(in: .whitespaces).isEmpty
    else {
      errormessage = "Please fill all the fields"
      return false
    }

    guard email.contains("@") && email.contains(".") else {

      errormessage = "enter valid email address"
      return false
    }

    return true
  }
}
