//
//  ToDoListApp.swift
//  ToDoList
//
//  Created by Ramya H G on 06/03/24.
//

import FirebaseCore
import SwiftData
import SwiftUI

@main
struct ToDoListApp: App {
  init() {
    FirebaseApp.configure()
  }

  var body: some Scene {
    WindowGroup {
      MainView()
    }
  }
}
