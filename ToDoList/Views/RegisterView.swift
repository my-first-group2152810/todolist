//
//  RegisterView.swift
//  ToDoList
//
//  Created by Ramya H G on 07/03/24.
//

import SwiftUI

struct RegisterView: View {
  
  @State var email = ""
  @State var name = ""
  @State var password = ""

  var body: some View {

    // Hedaer
    HeaderView(title: "Register Here", subtitle: "Organize todos", angle: -15, background: .yellow)

    // Create Account
    Form {

      TextField("Name", text: $name)
        .textFieldStyle(RoundedBorderTextFieldStyle())
      TextField("Email", text: $email)
        .textFieldStyle(RoundedBorderTextFieldStyle())
      SecureField("Password", text: $password)
        .textFieldStyle(RoundedBorderTextFieldStyle())
      TLButton(title: "Create Account", background: .blue) {
        // Action
      }.padding()

    }
    Spacer()

  }
}

#Preview {
  RegisterView()
}
