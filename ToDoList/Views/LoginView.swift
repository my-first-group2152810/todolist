//
//  LoginView.swift
//  ToDoList
//
//  Created by Ramya H G on 07/03/24.
//

import SwiftUI

struct LoginView: View {

  @StateObject var viewModel = LoginViewViewModel()

  var body: some View {
    NavigationView {
      VStack {
        // Header
        HeaderView(
          title: "To Do List", subtitle: "Get Things Done", angle: 15, background: .pink
        )

        // Login
        Form {
          if !viewModel.errormessage.isEmpty {
            Text(viewModel.errormessage)
              .foregroundColor(
                .red
              )
          }

          TextField("Email Address", text: $viewModel.email)
            .textFieldStyle(RoundedBorderTextFieldStyle())
          SecureField("Password", text: $viewModel.password)
            .textFieldStyle(RoundedBorderTextFieldStyle())

          TLButton(title: "Login", background: .blue) {
            // Action
            viewModel.login()

          }.padding()
        }

        VStack {
          Text("New arround here")
          NavigationLink("Register Here", destination: RegisterView())
        }.padding(.bottom, 50)
        Spacer()

      }

      // Create new Account

    }
  }

}

#Preview {
  LoginView()
}
