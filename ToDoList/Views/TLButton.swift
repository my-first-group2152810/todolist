//
//  TLButton.swift
//  ToDoList
//
//  Created by Ramya H G on 07/03/24.
//

import SwiftUI

struct TLButton: View {
  let title: String
  let background: Color
  let action: () -> Void
 static var Hello=5

// let H=5
    
  var body: some View {
    Button(
      action: {
        // Action
        action()

      },
      label: {
        ZStack {
           
          RoundedRectangle(cornerRadius: 10)
            .foregroundColor( /*@START_MENU_TOKEN@*/.blue /*@END_MENU_TOKEN@*/)
          Text(title)
            .foregroundColor(.white)
            .bold()
        }
      }
    )
  }
    
    //var Hello0 = 2
    
    func HELLO(){
        print("hello world")
    }
}

#Preview {
  TLButton(title: "Button", background: .blue) {
    // Action
  }
}
