//
//  HeaderView.swift
//  ToDoList
//
//  Created by Ramya H G on 07/03/24.
//

import SwiftUI

struct HeaderView: View {

  let title: String
  let subtitle: String
  let angle: Double
  let background: Color

  var body: some View {
    // Zstack is for overlaying the views
    ZStack {
      RoundedRectangle(cornerRadius: 10)
        .foregroundColor(background)
        .rotationEffect(Angle(degrees: angle))

      VStack {
        Text(title)
          .font(.system(size: 50))
          .foregroundColor(.white)
          .bold()
        Text(subtitle)
          .font(.system(size: 30))
          .foregroundColor(.white)
      }.padding(.top, 80)

    }
    .frame(
      width: UIScreen.main.bounds.width * 3,
      height: 350
    )
    //// To fit the width of zstack to entire screen
    .offset(y: -150)
    /// .rotationEffect(Angle(degrees: 15))  If we add rotation effect here all the elements in zstack rotates(incl text)
  }
}

#Preview {
  HeaderView(title: "Title", subtitle: "Subtitle", angle: 15, background: .blue)
}
